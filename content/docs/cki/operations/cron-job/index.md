---
title: Creating a cron job
description: >-
  Steps to do to automate a recurring task
companion: true
---

## Problem

You have a problem that can be helped by an automated recurring task.

## Steps to take

1. Create a Python script in [cki-tools] in the [cki-tools directory]. Make
   sure it

   - uses `argparse` to provide a proper CLI interface so it can be run locally
   - can be configured via environment variables
   - has a documentation page in the [documentation directory] that is linked
     from [README.md] that renders properly on the [main documentation site]
   - has proper unit tests that cover the common control flow, but also all the
     edge cases so that your tool can be extended and reworked by somebody else
     in the future
   - provides reasonable output about its operations via the `logging` package
     at the INFO level, and debugging output useful for development at the
     DEBUG level
   - initializes Sentry so that exceptions are properly recorded
   - is added to the smoke test commands for the cki-tools image in
     [.gitlab-ci.yml]

2. Configure the CronJob in the deployment repository, either by adding it to
   an existing `project-vars.yml.j2` or by adding a new file:

   ```yaml
   ---
   secret:
     - GITLAB_TOKEN: $COM_GITLAB_TOKEN_KPET_DB_SCHEDULE_SYNC_POLARION_ID_MR[deployed]
     - SENTRY_DSN: $CRONJOBS_SENTRY_DSN
   cronjobs:
     mr-weekly:
       schedule: "30 5 * * sun"
       image: quay.io/cki/cki-tools:production
       command: [cki_tools_sync_polarion_id_mr.sh]
       variables:
         - GITLAB_PROJECT_URL: gitlab.com/redhat/centos-stream/tests/kernel/kpet-db
         - GITLAB_FORK_URL: gitlab.com/redhat/centos-stream/tests/kernel/kpet-db
         - BRANCH_NAME: polarion-id-updates
         - GIT_USER_NAME: CKI Bot
         - GIT_USER_EMAIL: cki-project@redhat.com
   ```

   If adding a new project, add it to `.gitlab/ci_templates/jobs.yml` as well:

   ```yaml
   jobs:
     extends: .openshift_deploy
     parallel:
       matrix:
         - PROJECT_NAME:
            - sync-polarion-id
          PROJECT_CONTEXT:
            - cluster_name
          PROJECT_REGEX: '/^cki-tools$/'
   ```

   See the `README.md` file in the deployment repository for more details.

3. Wait for the deployment pipeline to pass all its linter jobs, and then
   trigger the `jobs` child pipeline. In the child pipeline, find your new job
   and trigger it. Watch it deploy, and then enter the CronJob environment via

   ```console
   $ oc --context cluster_name debug cj/sync-polarion-id-mr-weekly
   Starting pod/sync-polarion-id-mr-weekly-debug-csfh9, command was: ... cki_tools_sync_polarion_id_mr.sh
   Pod IP: 172.64.0.125
   sh-5.2# 
   ```

   Run the printed command manually to make sure it does what it is supposed to
   do.

4. After review and approval, merge the merge request.

{{% include "internal.md" %}}

[cki-tools]: https://gitlab.com/cki-project/cki-tools/
[cki-tools directory]: https://gitlab.com/cki-project/cki-tools/-/tree/main/cki_tools
[documentation directory]: https://gitlab.com/cki-project/cki-tools/-/tree/main/documentation
[README.md]: https://gitlab.com/cki-project/cki-tools/-/blob/main/README.md
[main documentation site]: https://cki-project.org/docs/background/cki-tools/
[.gitlab-ci.yml]: https://gitlab.com/cki-project/cki-tools/-/blob/main/.gitlab-ci.yml
