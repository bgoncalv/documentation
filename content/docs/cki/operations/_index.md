---
title: Operations handbook
linkTitle: Operations
description: Procedures for running and fixing the CKI setup
companion: true  # there are some internal-only operations docs
weight: 40
---
