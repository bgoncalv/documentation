---
title: Disabling zstream testing
description: >-
  Steps to do when a variant or a complete kernel stream drops out of support
companion: true
---

## Background

Every once in a while, RHEL streams drop out of support, and CKI needs to be
disabled there. This might concern the whole stream, or just a specific variant
like `rt`.

## Steps to take

1. Disable the relevant pipelines in `rh_metadata.yaml`.

   For complete streams, add `inactive: true` to the relevant `branch`.

   To disable only a specific variant, determine the default pipelines from the
   `project`, and add an override on the `branch` for that variable with the
   pipeline for the variant removed.

2. Remove the relevant pipelines from `.gitlab-ci.yml` in the correct branch in
   the kernel repository. Keep in mind that this file is also used in other
   derived variant-specific branches.

{{% include "internal.md" %}}
